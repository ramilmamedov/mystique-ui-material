FROM node:12

ARG api_host="https://api.worldazerbaijanis.com"
ENV NODE_ENV=production 
ENV REACT_APP_API=https://api.worldazerbaijanis.com

WORKDIR /usr/src/app
COPY package.json yarn.lock ./
RUN yarn

ENV PORT=3000
EXPOSE ${PORT}
COPY . ./
RUN yarn build
CMD ["node", "server.js"]