import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose, withState, withHandlers } from 'recompose';
//
import { REQUEST, actionCreator } from 'utils/config.actions';

import View from './view';
import { inputValueHandler } from 'utils/config.handlers';

export default compose(
  connect(state => state.Layout),
  withRouter,
  withState('state', 'setState', {
    email: '',
    password: '',
  }),
  withHandlers({
    inputValueHandler: inputValueHandler,
    submitLogin: ({ state, dispatch }) => e => {
      e.preventDefault();
      const data = { username: state.email, password: state.password };
      dispatch(actionCreator(REQUEST('LAYOUT', 'login'), data));
    },
  })
)(View);
