import React from 'react';
import {
  Grid,
  CircularProgress,
  Typography,
  withStyles,
  Button,
  TextField,
  Fade,
  FormControl,
  InputLabel,
} from '@material-ui/core';
import PasswordField from 'material-ui-password-field';

const Login = ({ classes, ...props }) => {
  const { state } = props;

  const btnDisabled = state.email === '' || state.password === '';

  return (
    <Grid container className={classes.container}>
      <form id="submitLogin" onSubmit={props.submitLogin}></form>
      <div className={classes.formContainer}>
        <div className={classes.form}>
          <Typography variant="h1" className={classes.greeting}>
            Welcome
          </Typography>
          <Fade in={props.failure_login}>
            <Typography color="secondary" className={classes.errorMessage}>
              Something is wrong with your Username or Password
            </Typography>
          </Fade>
          <TextField
            required
            fullWidth
            name="email"
            type="text"
            value={state.email}
            label="Email"
            placeholder="Email"
            className={classes.inputs}
            inputProps={{ autoComplete: 'new-password', form: 'submitLogin' }}
            onChange={props.inputValueHandler}
          />
          <FormControl fullWidth>
            <InputLabel htmlFor="password">Password</InputLabel>
            <PasswordField
              required
              fullWidth
              id="password"
              name="password"
              type="password"
              value={state.password}
              label="Password"
              placeholder="Password"
              className={classes.inputs}
              inputProps={{ autoComplete: 'new-password', form: 'submitLogin' }}
              onChange={props.inputValueHandler}
            />
          </FormControl>
          <div className={classes.formButtons}>
            {props.isLoading ? (
              <CircularProgress size={26} className={classes.loginLoader} />
            ) : (
              <Button
                size="large"
                form="submitLogin"
                color="primary"
                variant="contained"
                onClick={props.submitLogin}
                disabled={btnDisabled}
              >
                {' '}
                Login{' '}
              </Button>
            )}
          </div>
        </div>
        <Typography color="primary" className={classes.copyright}>
          Copyright © 2019. All Rights Reserved.
        </Typography>
      </div>
    </Grid>
  );
};

const styles = theme => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    top: 0,
    left: 0,
  },
  logotypeContainer: {
    backgroundColor: theme.palette.primary.main,
    width: '60%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    [theme.breakpoints.down('md')]: {
      width: '50%',
    },
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
  },
  formContainer: {
    width: '60%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    [theme.breakpoints.down('md')]: {
      width: '50%',
    },
  },
  inputs: { marginBottom: '20px' },
  form: { marginTop: theme.spacing(8) },
  tab: { fontWeight: 400, fontSize: 18 },
  greeting: {
    fontWeight: 500,
    textAlign: 'center',
    marginTop: theme.spacing(4),
    lineHeight: '1.4',
  },
  subGreeting: {
    fontWeight: 500,
    textAlign: 'center',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(4),
  },
  createAccountButton: { marginTop: '11px' },
  errorMessage: {
    textAlign: 'center',
    margin: '10px 0 20px',
  },
  formDividerContainer: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
    display: 'flex',
    alignItems: 'center',
  },
  formDividerWord: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
  formDivider: {
    flexGrow: 1,
    height: 1,
    backgroundColor: theme.palette.text.hint + '40',
  },
  textFieldUnderline: {
    '&:before': {
      borderBottomColor: theme.palette.primary.light,
    },
    '&:after': {
      borderBottomColor: theme.palette.primary.main,
    },
    '&:hover:before': {
      borderBottomColor: `${theme.palette.primary.light} !important`,
    },
  },
  textField: {
    borderBottomColor: theme.palette.background.light,
  },
  formButtons: {
    width: '100%',
    marginTop: theme.spacing(4),
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  loginLoader: { marginLeft: theme.spacing(4) },
  copyright: {
    textAlign: 'center',
    marginTop: theme.spacing(6),
    marginBottom: theme.spacing(6),
    whiteSpace: 'nowrap',
  },
});

export default withStyles(styles, { withTheme: true })(Login);
