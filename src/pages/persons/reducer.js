import { WRITE, REQUEST, SUCCESS, FAILURE } from 'utils/config.actions';
import qs from 'qs';

export const TYPE = `Persons`;
const query = qs.parse(window.location.search.split('?')[1]);

export const initialState = {
  create_modal: false,
  update_modal: false,
  filter_modal: false,
  data: [],
  columns: [],
  schema: [],
  pagination: {
    totalPages: 0,
    page: query.page || 1,
  },
  filter: query,
  //
  request_all: false,
  success_all: false,
  failure_all: false,
  //
  request_create: false,
  success_create: false,
  failure_create: false,
  //
  request_update: false,
  success_update: false,
  failure_update: false,
  //
  request_remove: false,
  success_remove: false,
  failure_remove: false,
  // image upload
  request_upload: false,
  success_upload: false,
  failure_upload: false,
};

const Persons = (state = initialState, { type, payload }) => {
  switch (type) {
    case WRITE(TYPE, 'open_crete_modal'):
      return {
        ...state,
        create_modal: true,
        update_modal: false,
        filter_modal: false,
      };
    case WRITE(TYPE, 'open_update_modal'):
      return {
        ...state,
        create_modal: false,
        update_modal: true,
        filter_modal: false,
      };
    case WRITE(TYPE, 'open_filter_modal'):
      return {
        ...state,
        create_modal: false,
        update_modal: false,
        filter_modal: true,
      };
    case WRITE(TYPE, 'close_modal'):
      return {
        ...state,
        create_modal: false,
        update_modal: false,
        filter_modal: false,
      };
    //
    case REQUEST(TYPE, 'all'):
      return {
        ...state,
        request_all: true,
      };
    case SUCCESS(TYPE, 'all'):
      return {
        ...state,
        request_all: false,
        success_all: true,
        data: payload.data,
        columns: payload.columns,
        schema: payload.schema,
        pagination: {
          ...state.pagination,
          ...payload.pagination,
        },
      };
    case FAILURE(TYPE, 'all'):
      return {
        ...state,
        request_all: false,
        failure_all: true,
      };
    //	Create
    case REQUEST(TYPE, 'create'):
      return {
        ...state,
        request_create: true,
        success_create: false,
      };
    case SUCCESS(TYPE, 'create'):
      return {
        ...state,
        request_create: false,
        success_create: true,
        create_modal: false,
        data: {
          ...state.data,
          profiles: [payload, ...state.data.profiles],
        },
      };
    case FAILURE(TYPE, 'create'):
      return {
        ...state,
        request_create: false,
        failure_create: true,
      };
    //	Update
    case REQUEST(TYPE, 'update'):
      return {
        ...state,
        request_update: true,
        success_update: false,
      };
    case SUCCESS(TYPE, 'update'):
      const updated = [...state.data.profiles];
      updated.map((item, index) => {
        if (item.pk === payload.pk) updated[index] = payload;
        return false;
      });
      return {
        ...state,
        request_update: false,
        success_update: true,
        update_modal: false,
        data: {
          ...state.data,
          profiles: updated,
        },
      };
    case FAILURE(TYPE, 'update'):
      return {
        ...state,
        request_update: false,
        failure_update: true,
      };
    case REQUEST(TYPE, 'upload'):
      return {
        ...state,
        request_upload: true,
      };
    case SUCCESS(TYPE, 'upload'):
      const profiles = [...state.data.profiles];
      profiles.map((item, index) => {
        if (item.pk === payload.pk) profiles[index].image_url = payload.image;
        return false;
      });
      return {
        ...state,
        request_upload: false,
        success_upload: true,
        data: {
          ...state.data,
          profiles: profiles,
        },
      };
    case FAILURE(TYPE, 'upload'):
      return {
        ...state,
        request_upload: false,
        failure_upload: true,
      };
    //	Delete
    case REQUEST(TYPE, 'remove'):
      return {
        ...state,
        request_remove: true,
        success_remove: false,
      };
    case SUCCESS(TYPE, 'remove'):
      const removed = state.data.profiles.filter(item => payload != item.pk);
      return {
        ...state,
        request_remove: false,
        success_remove: true,
        data: {
          ...state.data,
          profiles: removed,
        },
      };
    case FAILURE(TYPE, 'remove'):
      return {
        ...state,
        request_remove: false,
        failure_remove: true,
      };
    case SUCCESS(TYPE, 'search'):
      return {
        ...state,
        data: payload.data,
        pagination: {
          ...state.pagination,
          ...payload.pagination,
        },
      };
    case SUCCESS(TYPE, 'ADD_FILTERS'):
      return {
        ...state,
        data: payload.data,
        pagination: {
          ...state.pagination,
          ...payload.pagination,
        },
      };
    case 'ADD_FILTERS':
      return {
        ...state,
        filter: payload.filters,
      };
    case 'RESET_FILTERS':
      return {
        ...state,
        filter: {},
      };
    case 'PERSONS_PAGE_CHANGE':
      return {
        ...state,
        pagination: {
          ...state.pagination,
          page: payload.page,
        },
      };
    default:
      return state;
  }
};

export default Persons;
