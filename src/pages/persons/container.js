import { connect } from 'react-redux';
import { compose, withHandlers, withState } from 'recompose';
import qs from 'qs';
//
import View from './view';
import { TYPE } from './reducer';
import { WRITE, REQUEST, actionCreator } from 'utils/config.actions';
import { handleCreateUpdate, handleDelete } from 'utils/config.handlers';

export default compose(
  connect(state => state.Persons),
  withHandlers({
    handleCreate: handleCreateUpdate(TYPE, 'create'),
    handleUpdate: handleCreateUpdate(TYPE, 'update'),
    handleDelete: handleDelete(TYPE),
    //  Modal Handlers
    openCreateModal: ({ dispatch }) => () =>
      dispatch(actionCreator(WRITE(TYPE, 'open_crete_modal'))),
    openUpdateModal: ({ dispatch }) => () =>
      dispatch(actionCreator(WRITE(TYPE, 'open_update_modal'))),
    openFilterModal: ({ dispatch }) => () =>
      dispatch(actionCreator(WRITE(TYPE, 'open_filter_modal'))),
    closeModal: ({ dispatch }) => () =>
      dispatch(actionCreator(WRITE(TYPE, 'close_modal'))),
    serverSideSearch: ({ dispatch }) => (key, page) => {
      dispatch(actionCreator('PERSONS_PAGE_CHANGE', { page: 1 }));
      dispatch(
        actionCreator(REQUEST(TYPE, 'search'), { key: key, page: page })
      );
    },
    handleFilter: ({ dispatch }) => filters => {
      let perPage = process.env.REACT_APP_PERPAGE || 10;

      const query = qs.parse(window.location.search.split('?')[1]);
      const uri = qs.stringify({
        ...query,
        ...filters,
      });
      const api_uri = qs.stringify({
        ...query,
        ...filters,
        _from: 0,
        _size: perPage,
      });
      let newurl = window.location.pathname + `?${uri}`;
      window.history.pushState({ path: newurl }, '', newurl);

      dispatch(actionCreator('PERSONS_PAGE_CHANGE', { page: 1 }));
      dispatch({
        type: 'ADD_FILTERS',
        payload: { filters: filters },
      });
      dispatch(
        actionCreator(REQUEST(TYPE, 'ADD_FILTERS'), {
          filters: filters,
          page: 1,
          uri: api_uri,
        })
      );
    },
    resetFilter: ({ dispatch, closeModal }) => () => {
      let perPage = process.env.REACT_APP_PERPAGE || 10;
      const uri = qs.stringify({ page: 1 });
      dispatch({ type: 'RESET_FILTERS' });
      dispatch(actionCreator('PERSONS_PAGE_CHANGE', { page: 1 }));
      dispatch(
        actionCreator(REQUEST(TYPE, 'all'), {
          uri: qs.stringify({ _from: 0, _size: perPage }),
        })
      );
      let newurl = window.location.pathname + `?${uri}`;
      window.history.pushState({ path: newurl }, '', newurl);
    },
    pageHandler: ({ dispatch }) => ({ selected }) => {
      let newPage = selected + 1;
      let perPage = process.env.REACT_APP_PERPAGE || 10;
      let from = perPage * (newPage - 1);

      const query = qs.parse(window.location.search.split('?')[1]);
      const uri = qs.stringify({
        ...query,
        page: newPage,
      });
      const api_uri = qs.stringify({
        ...query,
        _from: from,
        _size: perPage,
      });
      let newurl = window.location.pathname + `?${uri}`;

      window.history.pushState({ path: newurl }, '', newurl);

      dispatch(actionCreator('PERSONS_PAGE_CHANGE', { page: newPage }));
      dispatch(actionCreator(REQUEST(TYPE, 'all'), { uri: api_uri }));
    },
    //	Submit
    submit: ({ dispatch }) => (method, hook, file) => {
      const { id, tableData, ...requestPayload } = hook;
      method === 'create'
        ? dispatch(
            actionCreator(REQUEST(TYPE, 'create'), {
              form: requestPayload,
              file: file,
            })
          )
        : dispatch(
            actionCreator(REQUEST(TYPE, 'update'), {
              form: requestPayload,
              file: file,
            })
          );
    },
    //  Lifecycle
    componentWillMount: ({ dispatch, pagination }) => () => {
      let perPage = process.env.REACT_APP_PERPAGE || 10;
      const query = qs.parse(window.location.search.split('?')[1]);
      let from = perPage * (pagination.page - 1);
      const api_uri = qs.stringify({
        ...query,
        _from: from,
        _size: perPage,
      });
      dispatch(actionCreator(REQUEST(TYPE, 'all'), { uri: api_uri }));
    },
  })
)(View);
