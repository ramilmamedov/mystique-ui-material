import { put, call, takeEvery } from 'redux-saga/effects';

import { TYPE } from './reducer';
import { apiGet, apiPost, apiPut, apiDelete } from 'utils/config.axios';
import {
  WRITE,
  REQUEST,
  SUCCESS,
  FAILURE,
  actionCreator,
} from 'utils/config.actions';

const endpoint = 'users/';

function* get(action) {
  try {
    let perPage = process.env.REACT_APP_PERPAGE || 10;

    const response_columns = yield call(apiGet, `${endpoint}schema/`);
    const response = yield call(apiGet, `${endpoint}?${action.payload.uri}`);
    function capitalizeFirstLetter(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }
    const schema = [...response_columns.data.fields];
    const columns = response_columns.data.fields
      .filter(field => !field.hidden)
      .sort((a, b) => a.display_order - b.display_order)
      .map(item => ({
        title: capitalizeFirstLetter(item.name),
        field: item.slug.toLowerCase(),
      }));
    yield put(
      actionCreator(SUCCESS(TYPE, 'all'), {
        data: response.data,
        pagination: {
          total: response.data.total,
          totalPages: Math.ceil(response.data.total / perPage),
        },
        columns: columns,
        schema: schema,
      })
    );
  } catch (error) {
    yield put(actionCreator(FAILURE(TYPE, 'all')));
    yield put(
      actionCreator(WRITE('Snackbar', 'show'), {
        message: `${error.response.status} - ${error.response.statusText}`,
        color: 'SnackbarRed',
      })
    );
  }
}

function* create(action) {
  try {
    const { form, file } = action.payload;
    const response = yield call(apiPost, endpoint, form);
    yield put(actionCreator(SUCCESS(TYPE, 'create'), response.data));
    if (file && !Array.isArray(file)) {
      yield put(
        actionCreator(REQUEST(TYPE, 'upload'), {
          id: response.data.pk,
          file: file,
        })
      );
    }
    yield put(
      actionCreator(WRITE('Snackbar', 'show'), {
        message: 'Yaradıldı!',
        color: 'SnackbarGreen',
      })
    );
  } catch (error) {
    yield put(actionCreator(FAILURE(TYPE, 'create')));
    yield put(
      actionCreator(WRITE('Snackbar', 'show'), {
        message: `${error.response.status} - ${error.response.statusText}`,
        color: 'SnackbarRed',
      })
    );
  }
}

function* update(action) {
  const { form, file } = action.payload;
  var id = action.payload.form.pk;
  delete form['pk'];
  delete form['image_url'];
  try {
    const response = yield call(apiPut, `${endpoint}${id}/`, form);

    if (file && !Array.isArray(file)) {
      yield put(
        actionCreator(REQUEST(TYPE, 'upload'), {
          id: response.data.pk,
          file: file,
        })
      );
    }
    yield put(actionCreator(SUCCESS(TYPE, 'update'), response.data));
    yield put(
      actionCreator(WRITE('Snackbar', 'show'), {
        message: 'Yeniləndi!',
        color: 'SnackbarGreen',
      })
    );
  } catch (error) {
    yield put(actionCreator(FAILURE(TYPE, 'update')));
    yield put(
      actionCreator(WRITE('Snackbar', 'show'), {
        message: `${error.response.status} - ${error.response.statusText}`,
        color: 'SnackbarRed',
      })
    );
  }
}

function* upload(action) {
  try {
    const { id, file } = action.payload;
    let formData = new FormData();
    if (file.status === 'done' && file.originFileObj) {
      formData.append('image', file.originFileObj);
    }
    const response = yield call(
      apiPost,
      `${endpoint}${id}/profile/picture/`,
      formData
    );
    yield put(
      actionCreator(SUCCESS(TYPE, 'upload'), { ...response.data, pk: id })
    );
  } catch (error) {
    yield put(actionCreator(FAILURE(TYPE, 'upload')));
    yield put(
      actionCreator(WRITE('Snackbar', 'show'), {
        message: `error`,
        color: 'SnackbarRed',
      })
    );
  }
}

function* remove(action) {
  try {
    yield call(apiDelete, `${endpoint}${action.payload}/`, action.payload);
    yield put(actionCreator(SUCCESS(TYPE, 'remove'), action.payload));
    yield put(
      actionCreator(WRITE('Snackbar', 'show'), {
        message: 'Silindi!',
        color: 'SnackbarGreen',
      })
    );
  } catch (error) {
    yield put(actionCreator(FAILURE(TYPE, 'remove')));
    yield put(
      actionCreator(WRITE('Snackbar', 'show'), {
        message: `${error.response.status} - ${error.response.data.detail}`,
        color: 'SnackbarRed',
      })
    );
  }
}

function* search(action) {
  try {
    let page = action.payload.page;
    let perPage = process.env.REACT_APP_PERPAGE || 10;
    let from = perPage * (page - 1);
    let url =
      action.payload.key.trim() === ''
        ? `${endpoint}?_from=${from}&_size=${perPage}`
        : `${endpoint}?fullname=${action.payload.key}&_from=${from}&_size=${perPage}`;
    const response = yield call(apiGet, url);
    yield put(
      actionCreator(SUCCESS(TYPE, 'search'), {
        data: response.data,
        pagination: {
          total: response.data.total,
          totalPages: Math.ceil(response.data.total / perPage),
        },
      })
    );
  } catch (error) {
    yield put(actionCreator(FAILURE(TYPE, 'search')));
    yield put(
      actionCreator(WRITE('Snackbar', 'show'), {
        message: `${error.response.status} - ${error.response.statusText}`,
        color: 'SnackbarRed',
      })
    );
  }
}

function* filter(action) {
  try {
    let perPage = process.env.REACT_APP_PERPAGE || 10;
    const response = yield call(apiGet, `${endpoint}?${action.payload.uri}`);

    yield put(
      actionCreator(SUCCESS(TYPE, 'ADD_FILTERS'), {
        data: response.data,
        pagination: {
          total: response.data.total,
          totalPages: Math.ceil(response.data.total / perPage),
        },
      })
    );
  } catch (error) {
    put(actionCreator(FAILURE(TYPE, 'ADD_FILTERS')));
    put(
      actionCreator(WRITE('Snackbar', 'show'), {
        message: `${error.response.status} - ${error.response.statusText}`,
        color: 'SnackbarRed',
      })
    );
  }
}

export default function* Persons() {
  yield takeEvery(REQUEST(TYPE, 'all'), get);
  yield takeEvery(REQUEST(TYPE, 'search'), search);
  yield takeEvery(REQUEST(TYPE, 'ADD_FILTERS'), filter);
  yield takeEvery(REQUEST(TYPE, 'create'), create);
  yield takeEvery(REQUEST(TYPE, 'update'), update);
  yield takeEvery(REQUEST(TYPE, 'remove'), remove);
  yield takeEvery(REQUEST(TYPE, 'upload'), upload);
}
