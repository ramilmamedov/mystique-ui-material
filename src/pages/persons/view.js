import React from 'react';
//
import Table from 'components/Table/Table';
import PageTitle from 'components/PageTitle/PageTitle';
import { TablePagination } from 'components/Pagination/TablePagination';

import FilterList from '@material-ui/icons/FilterList';
//
import CreateForm from 'components/Forms/CreateForm';
import UpdateForm from 'components/Forms/UpdateForm';
import FilterForm from 'components/Forms/FilterForm';

const Persons = ({
  pagination,
  data,
  columns,
  schema,
  submit,
  create_modal,
  update_modal,
  filter_modal,
  request_create,
  request_update,
  request_all,
  handleCreate,
  handleUpdate,
  handleFilter,
  handleDelete,
  serverSideSearch,
  componentWillMount,
  openCreateModal,
  openUpdateModal,
  openFilterModal,
  closeModal,
  pageHandler,
  filter,
  resetFilter,
}) => {
  const [rowData, setRowData] = React.useState({});
  React.useEffect(() => {
    componentWillMount();
  }, [componentWillMount]);
  const editable = React.useMemo(
    () => ({
      // onRowAdd: handleCreate,
      // onRowUpdate: handleUpdate,
      onRowDelete: handleDelete,
    }),
    [handleCreate, handleUpdate, handleDelete]
  );

  return (
    <React.Fragment>
      <PageTitle title={'Persons'} />
      <FilterForm
        handleFilter={handleFilter}
        schema={schema}
        closeModal={closeModal}
        open={filter_modal}
        filter={filter}
        reset={resetFilter}
      />
      <Table
        title="Persons"
        data={data.profiles}
        columns={columns}
        editable={editable}
        isLoading={request_all}
        onRowClick={(event, rowData) =>
          (window.location.href = `/app/persons/${rowData.pk}`)
        }
        components={{
          Pagination: () => (
            <TablePagination
              total={pagination.totalPages}
              pageHandler={pageHandler}
              page={parseInt(pagination.page) - 1}
            />
          ),
        }}
        actions={[
          {
            icon: 'add',
            tooltip: 'Add Modal',
            isFreeAction: true,
            onClick: () => openCreateModal(),
          },
          {
            icon: 'create',
            tooltip: 'Edit Modal',
            onClick: (e, rowData) => {
              openUpdateModal();
              setRowData(rowData);
            },
          },
          {
            icon: FilterList,
            tooltip: 'Filter',
            isFreeAction: true,
            onClick: (e, rowData) => {
              openFilterModal();
            },
          },
        ]}
        serverSideSearch={key => serverSideSearch(key, pagination.page)}
      />
      <CreateForm
        title="Create Form"
        submit={submit}
        columns={schema}
        open={create_modal}
        loader={request_create}
        closeModal={closeModal}
      />
      <UpdateForm
        title="Update Form"
        rowData={rowData}
        submit={submit}
        columns={schema}
        open={update_modal}
        loader={request_update}
        closeModal={closeModal}
      />
    </React.Fragment>
  );
};

export default React.memo(Persons);
