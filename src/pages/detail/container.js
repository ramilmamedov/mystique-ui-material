import { connect } from 'react-redux';
import { compose, withHandlers, withState, lifecycle } from 'recompose';
//
import View from './view';
import { TYPE } from './reducer';
import { WRITE, REQUEST, actionCreator } from 'utils/config.actions';

export default compose(
  connect(state => ({ detail: state.Detail })),
  withHandlers({
    componentWillMount: ({ dispatch, ...props }) => () => {
      dispatch(
        actionCreator(REQUEST(TYPE, 'profile'), { id: props.match.params.id })
      );
    },
  })
)(View);
