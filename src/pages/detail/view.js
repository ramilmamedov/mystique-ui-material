import React, { useRef } from 'react';
import { withStyles, Paper, Grid, Hidden } from '@material-ui/core';
import DetailTable from 'components/Table/DetailTable';
import { Button } from 'antd';
import { Print } from 'react-easy-print';

const View = ({ classes, componentWillMount, detail }) => {
  React.useEffect(() => {
    componentWillMount();
  }, [componentWillMount]);

  console.log(detail);

  return (
    <>
      <Print single="true">
        <Paper className={classes.paper}>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={6} md={4}>
              <div className={classes.imgBox}>
                <img src={detail.data.image_url} />
              </div>
            </Grid>
            <Grid item xs={12} sm={6} md={8}>
              <DetailTable data={detail.data} schema={detail.schema} />
            </Grid>
          </Grid>
        </Paper>
      </Print>
    </>
  );
};

const styles = theme => ({
  paper: {
    padding: '30px',
  },
  imgBox: {
    width: 'auto',
    maxWidth: '100%',
    height: 'auto',
    padding: '1px',
    overflow: 'hidden',
    borderRadius: '4px',
    '& img': {
      minWidth: '100%',
    },
  },
});

export default withStyles(styles, { withTheme: true })(View);
