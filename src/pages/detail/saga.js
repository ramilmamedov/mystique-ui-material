import { put, call, takeEvery } from 'redux-saga/effects';

import { TYPE } from './reducer';
import { apiGet } from 'utils/config.axios';
import {
  WRITE,
  REQUEST,
  SUCCESS,
  FAILURE,
  actionCreator,
} from 'utils/config.actions';

function* get(action) {
  try {
    const schema = yield call(apiGet, `/users/schema`);
    const res = yield call(apiGet, `/users/${action.payload.id}`);
    yield put(
      actionCreator(SUCCESS(TYPE, 'profile'), {
        profile: res.data,
        schema: schema.data.fields,
      })
    );
  } catch (error) {
    yield put(actionCreator(FAILURE(TYPE, 'profile')));
    yield put(
      actionCreator(WRITE('Snackbar', 'show'), {
        message: `${error.response.status} - ${error.response.statusText}`,
        color: 'SnackbarRed',
      })
    );
  }
}

export default function* Detail() {
  yield takeEvery(REQUEST(TYPE, 'profile'), get);
}
