import { WRITE, REQUEST, SUCCESS, FAILURE } from 'utils/config.actions';

export const TYPE = `Detail`;

export const initialState = {
  request: true,
  success: false,
  failure: false,
  data: {},
  schema: [],
};

const Detail = (state = initialState, { type, payload }) => {
  switch (type) {
    case REQUEST(TYPE, 'profile'):
      return {
        ...state,
        request: true,
        success: false,
        failure: false,
      };
    case SUCCESS(TYPE, 'profile'):
      return {
        ...state,
        request: false,
        success: true,
        failure: false,
        data: payload.profile,
        schema: payload.schema,
      };
    case FAILURE(TYPE, 'profile'):
      return {
        ...state,
        request: false,
        success: false,
        failure: true,
      };
    default:
      return state;
  }
};

export default Detail;
