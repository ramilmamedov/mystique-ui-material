import { combineReducers } from 'redux';

import Layout from 'components/Layout/reducer';
import Snackbar from 'components/Snackbar/reducer';
//
import Persons from 'pages/persons/reducer';
import Detail from 'pages/detail/reducer';

export default combineReducers({
  Layout,
  Snackbar,
  //
  Persons,
  Detail,
});
