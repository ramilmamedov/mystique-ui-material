import { all } from 'redux-saga/effects';

import Layout from 'components/Layout/saga';
//
import Persons from 'pages/persons/saga';
import Detail from 'pages/detail/saga';

export default function* rootSaga() {
  yield all([
    Layout(),
    //
    Persons(),
    Detail(),
  ]);
}
