export const actionCreator = (actionType, payload = undefined) => ({ type: actionType, payload });

export const
  WRITE = (content_type, method) => `WRITE_${content_type}: ${method}`,
  DELETE = (content_type, method) => `DELETE_${content_type}: ${method}`,
  REQUEST = (content_type, method) => `REQUEST_${content_type}: ${method}`,
  SUCCESS = (content_type, method) => `SUCCESS_${content_type}: ${method}`,
  FAILURE = (content_type, method) => `FAILURE_${content_type}: ${method}`;

