import axios from 'axios';
import store from '../redux/store';
import { WRITE, actionCreator } from 'utils/config.actions';

const AccessToken = localStorage.getItem('token-access');
const RefreshToken = localStorage.getItem('token-refresh');

export let axiosInstance = axios.create({
  baseURL: `${process.env.REACT_APP_API}/` || 'http://localhost:3000/',
  headers: {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${AccessToken}`,
  },
});

axiosInstance.interceptors.response.use(
  response => response,
  error => {
    const originalRequest = error.config;
    if (error.response.status === 400) {
      store.dispatch(
        actionCreator(WRITE('Snackbar', 'show'), {
          message: `${error.response.status} - ${error.response.statusText}`,
          color: 'SnackbarRed',
        })
      );
    }
    if (error.response.status === 401 && !originalRequest._retry) {
      originalRequest._retry = true;

      return axios
        .post(
          `${process.env.REACT_APP_API}/api/refresh/`,
          { refresh: RefreshToken },
          {
            headers: {
              'Content-Type': 'application/json',
            },
          }
        )
        .then(({ data }) => {
          axios.defaults.headers.common[
            'Authorization'
          ] = `Bearer ${data.access}`;
          axios.defaults.headers.common['Content-Type'] = 'application/json';
          originalRequest.headers['Authorization'] = `Bearer ${data.access}`;
          originalRequest.headers['Content-Type'] = 'application/json';
          localStorage.setItem('token-access', data.access);
          axiosInstance = axios.create({
            baseURL:
              `${process.env.REACT_APP_API}/` || 'http://localhost:3000/',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${data.access}`,
            },
          });
          return axios(originalRequest);
        })
        .catch(err => {
          localStorage.removeItem('token-access');
          localStorage.removeItem('token-refresh');
          window.localtion.href = '/login';
        });
    }
    return Promise.reject(error);
  }
);

export const apiGet = async endpoint => {
  try {
    const response = await axiosInstance.get(endpoint);
    return response;
  } catch (error) {
    throw error;
  }
};

export const apiPost = async (endpoint, data) => {
  try {
    const fileConfig = Array.isArray(data)
      ? {
          headers: {
            'Content-Type': `multipart/form-data; boundary=${data._boundary}`,
          },
        }
      : {};
    const response = await axiosInstance.post(endpoint, data, fileConfig);
    return response;
  } catch (error) {
    throw error;
  }
};

export const apiPut = async (endpoint, data) => {
  try {
    const response = await axiosInstance.put(endpoint, data);
    return response;
  } catch (error) {
    throw error;
  }
};

export const apiDelete = async (endpoint, data) => {
  try {
    const response = await axiosInstance.delete(endpoint, data);
    return response;
  } catch (error) {
    throw error;
  }
};
