import { REQUEST, actionCreator } from './config.actions';

//	Input's Value Writing to State
export const inputValueHandler = ({ state, setState }) => ({ target }) => {
  setState({
    ...state,
    [target.name]: target.type === 'checkbox' ? target.checked : target.value,
  });
};

//  Table Create Row
export const handleCreateUpdate = (TYPE, method) => ({ dispatch }) => newData =>
  new Promise(resolve => {
    setTimeout(() => {
      const { id, tableData, ...requestPayload } = newData;

      method === 'create'
        ? dispatch(actionCreator(REQUEST(TYPE, 'create'), requestPayload))
        : dispatch(
            actionCreator(REQUEST(TYPE, 'update'), {
              id: id,
              data: requestPayload,
            })
          );
      resolve();
    }, 800);
  });

//  Table Delete Row
export const handleDelete = TYPE => ({ dispatch }) => oldData =>
  new Promise(resolve => {
    dispatch(actionCreator(REQUEST(TYPE, 'remove'), oldData.pk));
    resolve();
  });
