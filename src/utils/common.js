import qs from 'qs';

export const queryBuilder = fields => {
  let kv = Object.entries(fields);
  let query = '';
  kv.map(([k, v]) => {
    return (query += `&${k}=${v}`);
  });
  return query;
};

export const getPageFromUrl = () =>
  qs.parse(window.location.search.split('?')[1]).page || 1;
