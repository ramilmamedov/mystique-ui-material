import { compose, withState, withHandlers } from 'recompose';
import { connect } from 'react-redux';

import View from './view';
import { toggleSidebar } from '../Layout/reducer';

export default compose(
  connect(state => state.Layout, { toggleSidebar }),
  withState('profileMenu', 'setProfileMenu', null),
  withHandlers({
    logout: () => () => {
      localStorage.clear();
      window.location.reload(false);
    },
    closeProfileMenu: ({ setProfileMenu }) => () => setProfileMenu(null),
    openProfileMenu: ({ setProfileMenu }) => e => setProfileMenu(e.currentTarget),
  })
)(View);
