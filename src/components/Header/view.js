import React from 'react';
import classNames from 'classnames';

import {
  AppBar,
  Toolbar,
  Button,
  IconButton,
  Menu,
  // MenuItem,
  withStyles,
  Typography,
} from '@material-ui/core';

// import { DateRange, Phone } from '@material-ui/icons';
import { Menu as MenuIcon, Person, ArrowBack } from '@material-ui/icons';

const Header = ({
  classes,
  isSidebarOpened,
  agentData,
  toggleSidebar,
  ...props
}) => (
  <AppBar position="fixed" className={classes.appBar}>
    <Toolbar className={classes.toolbar}>
      <IconButton
        color="inherit"
        onClick={toggleSidebar}
        className={classNames(
          classes.headerMenuButton,
          classes.headerMenuButtonCollapse
        )}
      >
        {isSidebarOpened ? (
          <ArrowBack
            classes={{
              root: classNames(classes.headerIcon, classes.headerIconCollapse),
            }}
          />
        ) : (
          <MenuIcon
            classes={{
              root: classNames(classes.headerIcon, classes.headerIconCollapse),
            }}
          />
        )}
      </IconButton>
      <Typography variant="h6" weight="medium" className={classes.logotype}>
        World Azerbaijanis
      </Typography>
      <div className={classes.grow} />
      <IconButton
        aria-haspopup="true"
        color="inherit"
        className={classes.headerMenuButton}
        aria-controls="profile-menu"
        onClick={props.openProfileMenu}
      >
        <Person classes={{ root: classes.headerIcon }} />
      </IconButton>
      <Menu
        id="profile-menu"
        open={Boolean(props.profileMenu)}
        anchorEl={props.profileMenu}
        onClose={props.closeProfileMenu}
        className={classes.headerMenu}
        classes={{ paper: classes.profileMenu }}
        disableAutoFocusItem
      >
        {/* <div className={classes.profileMenuUser}>
          <Typography variant="h4" weight="medium">
            {agentData.name} {agentData.surname}
          </Typography>
        </div> */}
        {/* <MenuItem
          className={classNames(
            classes.profileMenuItem,
            classes.headerMenuItem
          )}
        >
          <Phone className={classes.profileMenuIcon} />
          {agentData.phoneNumber}
        </MenuItem>
        <MenuItem
          className={classNames(
            classes.profileMenuItem,
            classes.headerMenuItem
          )}
        >
          <DateRange className={classes.profileMenuIcon} />
          {agentData.birthday}
        </MenuItem> */}
        <div className={classes.profileMenuUser}>
          <Button
            variant="outlined"
            color="primary"
            className={classes.signOut}
            onClick={props.logout}
          >
            Sign Out
          </Button>
        </div>
      </Menu>
    </Toolbar>
  </AppBar>
);

const styles = theme => ({
  logotype: {
    color: 'white',
    marginLeft: theme.spacing(2.5),
    marginRight: theme.spacing(2.5),
    fontWeight: 500,
    fontSize: 18,
    whiteSpace: 'nowrap',
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  appBar: {
    width: '100vw',
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  toolbar: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
  hide: { display: 'none' },
  grow: { flexGrow: 1 },
  headerMenu: { marginTop: theme.spacing(7) },
  headerMenuItem: {
    '&:hover, &:focus': {
      backgroundColor: theme.palette.primary.main,
      color: 'white',
    },
  },
  headerMenuButton: {
    marginLeft: theme.spacing(2),
    padding: theme.spacing(0.5),
  },
  headerMenuButtonCollapse: { marginRight: theme.spacing(2) },
  headerIcon: {
    fontSize: 28,
    color: 'rgba(255, 255, 255, 0.35)',
  },
  headerIconCollapse: { color: 'white' },
  profileMenu: { minWidth: 265 },
  profileMenuUser: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(2),
  },
  profileMenuIcon: { marginRight: theme.spacing(2) },
  profileMenuLink: {
    fontSize: 16,
    textDecoration: 'none',
    '&:hover': { cursor: 'pointer' },
  },
  signOut: { display: 'block' },
});

export default withStyles(styles)(Header);
