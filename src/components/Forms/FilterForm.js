import React, { useEffect } from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import {
  Button,
  CircularProgress,
  TextField,
  Dialog,
  DialogContent,
  DialogTitle,
} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(1),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    divider: {
      margin: theme.spacing(2, 0),
    },
    ml: {
      marginLeft: '10px',
    },
  })
);

export default function FilterForm({
  handleFilter,
  schema,
  open,
  closeModal,
  filter,
  reset,
}) {
  const classes = useStyles();

  const [formState, setFormState] = React.useState(filter);
  useEffect(() => {
    setFormState(filter);
  }, [filter]);

  let fields = schema.map(field => ({
    name: field.name,
    label: field.name,
    placeholder: field.name,
    type: field.datatype == 'float' ? 'number' : field.datatype,
  }));

  const handleChange = ({ target }) => {
    setFormState({ ...formState, [target.name]: target.value });
  };

  const handleSubmit = e => {
    e.preventDefault();
    closeModal();
    handleFilter(formState);
  };

  const resetFilter = () => {
    reset();
    closeModal();
  };

  return (
    <Dialog
      open={open}
      scroll="body"
      onClose={closeModal}
      fullWidth={true}
      aria-labelledby="scroll-dialog-title"
    >
      <DialogContent>
        <div className={classes.root}>
          <form action="" method="POST" onSubmit={handleSubmit}>
            <Grid container spacing={1}>
              {fields.map((field, i) => (
                <Grid item xs={3} key={i}>
                  <TextField
                    fullWidth
                    type={field.type}
                    name={field.name}
                    label={field.label}
                    onChange={handleChange}
                    defaultValue={formState[field.name]}
                    placeholder={field.placeholder}
                    inputProps={{
                      step: 0.01,
                    }}
                  />
                </Grid>
              ))}
            </Grid>
            <div className={classes.divider} />
            <Grid container spacing={1} justify={'flex-end'}>
              <Button
                type="button"
                color="primary"
                variant="contained"
                className={classes.mainButton}
                onClick={resetFilter}
              >
                Reset
              </Button>
              <Button
                type="submit"
                color="primary"
                variant="contained"
                className={(classes.mainButton, classes.ml)}
              >
                Filter
              </Button>
            </Grid>
            <div className={classes.divider} />
          </form>
        </div>
      </DialogContent>
    </Dialog>
  );
}
