import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid, Button, TextField, CircularProgress } from '@material-ui/core';
//
import DialogForm from 'components/DialogForm/DialogForm';
import ImageUpload from '../Upload/Upload';

const CreateForm = ({ submit, title, loader, columns, open, closeModal }) => {
  // Hooks
  const classes = useStyles();
  const [image, setImage] = React.useState([]);
  const [hook, setHook] = React.useState({});
  const [inputs, setInput] = React.useState([]);

  React.useEffect(() => {
    setHook({});
    setInput(columns);
  }, [columns, open]);

  const fields = inputs
    .sort((a, b) => a.display_order - b.display_order)
    .map(field => ({
      name: field.slug,
      label: field.name,
      placeholder: field.name,
      value: '',
      type: field.datatype == 'float' ? 'number' : field.datatype,
    }));

  const inputValueHandler = ({ target }) => {
    setHook({ ...hook, [target.name]: target.value });
  };

  return (
    <DialogForm
      title={title}
      open={open}
      closeModal={closeModal}
      Buttons={
        <Button
          color="primary"
          variant="contained"
          onClick={() => submit('create', hook, image)}
          className={classes.mainButton}
        >
          {loader ? (
            <CircularProgress color="secondary" size={20} />
          ) : (
            'Təstiq Et'
          )}
        </Button>
      }
    >
      <ImageUpload onChange={setImage} files={image} />
      <Grid container spacing={4} justify="space-between">
        {fields.map((field, i) => (
          <Grid item xs={12} key={i}>
            <TextField
              required
              fullWidth
              type={field.type}
              name={field.name}
              label={field.label}
              onChange={inputValueHandler}
              defaultValue={hook[field.name]}
              placeholder={field.placeholder}
            />
          </Grid>
        ))}
      </Grid>
    </DialogForm>
  );
};

const useStyles = makeStyles({
  mainButton: { textTransform: 'capitalize', width: '100px', height: '36px' },
});

export default React.memo(CreateForm);
