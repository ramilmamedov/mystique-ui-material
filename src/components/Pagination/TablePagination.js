import React from 'react';
import ReactPaginate from 'react-paginate';
import { Link } from 'react-router-dom';

export const TablePagination = ({ total, pageHandler, page }) => {
  return (
    <div className={'pagination-container'}>
      <ReactPaginate
        pageCount={total}
        onPageChange={page => pageHandler(page)}
        initialPage={page}
        disableInitialCallback={true}
        pageRangeDisplayed={3}
        marginPagesDisplayed={3}
        containerClassName="pagination"
        pageClassName="page-item"
        pageLinkClassName="page-link"
        activeClassName="active"
        previousClassName="page-item"
        nextClassName="page-item"
        previousLinkClassName="page-link"
        nextLinkClassName="page-link"
        disabledClassName="disabled"
      />
    </div>
  );
};
