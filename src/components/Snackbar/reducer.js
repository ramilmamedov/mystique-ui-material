import { WRITE, DELETE } from 'utils/config.actions'

export const TYPE = `Snackbar`

export const initialState = { snackbarMessage: '', snackbarColor: '', snackbarState: false }

const Snackbar = (state = initialState, { type, payload }) => {
  switch (type) {
    case WRITE(TYPE, 'show'):
      return {
        ...state,
        snackbarState: true,
        snackbarMessage: payload.message,
        snackbarColor: payload.color,
      }
    case DELETE(TYPE, 'hide'):
      return {
        ...state,
        snackbarState: false,
        snackbarMessage: '',
      }
    default:
      return state
  }
}

export default Snackbar
