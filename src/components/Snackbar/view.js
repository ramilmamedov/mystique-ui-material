import React from 'react'

import CloseIcon from '@material-ui/icons/Close'
import { makeStyles } from '@material-ui/core/styles'
import { Snackbar, IconButton } from '@material-ui/core'

const SimpleSnackbar = ({ snackbarState, snackbarColor, snackbarMessage, closeSnackbar }) => {
  const classes = useStyles();
  return (
    <Snackbar
      className={snackbarColor}
      open={snackbarState}
      onClose={closeSnackbar}
      message={<span id="message-id">{snackbarMessage}</span>}
      ContentProps={{ 'aria-describedby': 'message-id' }}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
      autoHideDuration={3000}
      action={[
        <IconButton key="close" aria-label="close" onClick={closeSnackbar} className={classes.close} >
          <CloseIcon />
        </IconButton>
      ]}
    />
  )
}

const useStyles = makeStyles(theme => ({ close: { padding: theme.spacing(0.5) } }))

export default React.memo(SimpleSnackbar);
