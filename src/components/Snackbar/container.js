import { connect } from 'react-redux'
import { compose, withHandlers } from 'recompose'

import View from './view'
import { TYPE } from './reducer'
import { DELETE, actionCreator } from 'utils/config.actions'

export default compose(
  connect(state => state.Snackbar),
  withHandlers({
    closeSnackbar: ({ dispatch }) => () => dispatch(actionCreator(DELETE(TYPE, 'hide'))),
  })
)(View);