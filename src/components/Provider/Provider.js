import React from 'react';

import { Select, MenuItem, InputLabel, FormControl, withStyles } from '@material-ui/core';

const Provider = ({ classes, ...props }) => {
  let numbers = [];
  props.version === 'mobile' ?
    numbers = ['050', '051', '055', '070', '077'] :
    numbers = ['012', '018', '020', '021', '022', '023', '024', '025', '026', '036', '060'];

  return (
    <FormControl className={props.className ? props.className : classes.selectStyle}>
      <InputLabel htmlFor="Provider">Provider</InputLabel>
      <Select
        value={props.state}
        onChange={props.inputValueHandler}
        inputProps={{ name: props.name }}
      >
        {numbers.map(number => <MenuItem value={number} key={number}>{number}</MenuItem>)}
      </Select>
    </FormControl>
  );
}

const styles = ({ selectStyle: { width: '20%', marginBottom: '0' } });

export default withStyles(styles)(Provider);