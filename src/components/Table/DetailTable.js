import React from 'react';
import { withStyles } from '@material-ui/core';

const DetailTable = ({ classes, data, schema }) => {
  return (
    <table className={classes.table}>
      <tbody>
        {schema
          .sort((a, b) => a.display_order - b.display_order)
          .map(field => (
            <tr className={classes.tr} key={field.name}>
              <th className={classes.th}>{field.name}</th>
              <td className={classes.td}>{data[field.slug]}</td>
            </tr>
          ))}
      </tbody>
    </table>
  );
};

const styles = theme => ({
  table: {
    width: '100%',
  },
  th: {
    padding: '15px',
    maxWidth: '200px',
    width: '200px',
    fontSize: '17px',
  },
  td: {
    padding: '15px',
    fontSize: '17px',
  },
  tr: {
    borderBottom: '1px solid #eee',
  },
});

export default withStyles(styles, { withTheme: true })(DetailTable);
