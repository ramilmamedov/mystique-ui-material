import React, { forwardRef } from 'react';
import { Grid } from '@material-ui/core';
import MaterialTable from 'material-table';
//
import PageTitle from 'components/PageTitle/PageTitle';
//
import { localization } from 'utils/config.localization';
import FilterForm from 'components/Forms/FilterForm';
import FilterList from '@material-ui/icons/FilterList';

const Table = ({
  data,
  columns,
  options,
  actions,
  editable,
  serverSideSearch,
  isLoading,
  components,
  onRowClick,
}) => (
  <React.Fragment>
    <Grid container spacing={4}>
      <Grid item xs={12}>
        <MaterialTable
          title=""
          data={data}
          columns={columns}
          actions={actions}
          isLoading={isLoading}
          localization={localization}
          onSearchChange={e => serverSideSearch(e)}
          components={{ ...components }}
          options={{
            ...options,
            pageSize: 10,
            filtering: false,
            exportButton: true,
            draggable: false,
            loadingType: 'overlay',
            thirdSortClick: false,
            addRowPosition: 'first',
            actionsColumnIndex: -1,
            searchFieldAlignment: 'left',
            actionsCellStyle: { padding: '4px 16px' },
          }}
          editable={editable}
          onRowClick={onRowClick}
        />
      </Grid>
    </Grid>
  </React.Fragment>
);

export default Table;
