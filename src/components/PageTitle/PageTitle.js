import React from 'react';
import { Typography, withStyles } from '@material-ui/core';

const PageTitle = ({ classes, ...props }) => (
  <Typography className={classes.pageTitleContainer} variant="h1" size="sm">
    {props.title}
  </Typography>
);

const styles = theme => ({
  pageTitleContainer: {
    display: 'flex',
    fontWeight: '400',
    fontSize: '2.5rem',
    justifyContent: 'space-between',
    marginBottom: theme.spacing(4),
    marginTop: theme.spacing(2),
    color: theme.palette.text.hint,
  },
  button: {
    boxShadow: theme.customShadows.widget,
    textTransform: 'none',
    '&:active': { boxShadow: theme.customShadows.widgetWide },
  },
});

export default withStyles(styles)(PageTitle);
