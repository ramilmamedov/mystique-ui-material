import React from 'react';
import classNames from 'classnames';
import { Person, ArrowBack } from '@material-ui/icons';
import { Drawer, IconButton, List, withStyles } from '@material-ui/core';
//
import SidebarLink from 'components/SidebarLink/container';

const structure = [{ id: 1, label: 'Persons', link: '/app/persons', icon: <Person /> }];

const SidebarView = ({ classes, toggleSidebar, isSidebarOpened, isPermanent, location }) => {
  return (
    <Drawer
      variant={isPermanent ? 'permanent' : 'temporary'}
      className={classNames(classes.drawer, { [classes.drawerOpen]: isSidebarOpened, [classes.drawerClose]: !isSidebarOpened })}
      classes={{ paper: classNames({ [classes.drawerOpen]: isSidebarOpened, [classes.drawerClose]: !isSidebarOpened, }) }}
      open={isSidebarOpened}
    >
      <div className={classes.toolbar} />
      <div className={classes.mobileBackButton}>
        <IconButton onClick={toggleSidebar}><ArrowBack classes={{ root: classNames(classes.headerIcon, classes.headerIconCollapse), }} /></IconButton>
      </div>
      <List className={classes.sidebarList}>
        {structure.map(link => <SidebarLink key={link.id} location={location} isSidebarOpened={isSidebarOpened} {...link} />)}
      </List>
    </Drawer>
  );
};

const drawerWidth = 240;

const styles = theme => ({
  menuButton: { marginLeft: 12, marginRight: 36, },
  hide: { display: 'none', },
  drawer: { width: drawerWidth, flexShrink: 0, whiteSpace: 'nowrap', },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(12),
    [theme.breakpoints.down('sm')]: {
      width: drawerWidth,
    },
  },
  toolbar: {
    ...theme.mixins.toolbar,
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  content: { flexGrow: 1, padding: theme.spacing(3), },
  sidebarList: { marginTop: theme.spacing(3), },
  mobileBackButton: {
    marginTop: theme.spacing(0.5),
    marginLeft: theme.spacing(3),
    [theme.breakpoints.only('sm')]: {
      marginTop: theme.spacing(0.625),
    },
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
});

export default withStyles(styles, { withTheme: true })(SidebarView);
