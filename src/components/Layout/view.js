import React from 'react';
import classnames from 'classnames';
import { withStyles, CssBaseline } from '@material-ui/core';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
//
import Persons from 'pages/persons/container';
//
import Header from 'components/Header/container';
import Snackbar from 'components/Snackbar/container';
//
import Sidebar from '../Sidebar/container';
import Detail from '../../pages/detail/container';
import PrintProvider, { NoPrint } from 'react-easy-print';

const Layout = ({ classes, isSidebarOpened }) => (
  <PrintProvider>
    <NoPrint>
      <div className={classes.root}>
        <CssBaseline />
        <BrowserRouter>
          <Header />
          <Sidebar />
          <div
            className={classnames(classes.content, {
              [classes.contentShift]: isSidebarOpened,
            })}
          >
            <div className={classes.fakeToolbar} />
            <Switch>
              <Route path="/app/persons" exact component={Persons} />
              <Route path="/app/persons/:id" component={Detail} />
            </Switch>
            <Snackbar />
          </div>
        </BrowserRouter>
      </div>
    </NoPrint>
  </PrintProvider>
);

const styles = theme => ({
  root: {
    display: 'flex',
    maxWidth: '100vw',
    overflowX: 'hidden',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    width: `calc(100vw - 240px)`,
    minHeight: '100vh',
  },
  contentShift: {
    width: `calc(100vw - ${240 + theme.spacing(6)}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  fakeToolbar: { ...theme.mixins.toolbar },
});

export default withStyles(styles)(Layout);
