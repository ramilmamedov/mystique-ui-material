import { put, call, takeEvery } from 'redux-saga/effects';

import { TYPE } from './reducer';
import { apiPost } from 'utils/config.axios';
import { WRITE, REQUEST, SUCCESS, FAILURE, actionCreator } from 'utils/config.actions';

function* login(action) {
	try {
		const response = yield call(apiPost, 'api/token/', action.payload);
		if ('access' in response.data) {
			yield put(actionCreator(SUCCESS(TYPE, 'login')));
			localStorage.setItem('token-access', response.data.access);
			localStorage.setItem('token-refresh', response.data.refresh);
			window.location.reload(false);
		} else {
			yield put(actionCreator(FAILURE(TYPE, 'login')));
		}
	} catch (error) {
		yield put(actionCreator(FAILURE(TYPE, 'login')));
		yield put(actionCreator(WRITE('SNACKBAR', 'show'), { message: error.response.data.message, color: 'SnackbarRed' }));
	}
}

export default function* Layout() {
	yield takeEvery(REQUEST(TYPE, 'login'), login);
}
