import { compose, lifecycle } from 'recompose';
import { connect } from 'react-redux';

import View from './view';
import { TYPE } from './reducer';
import { REQUEST, actionCreator } from 'utils/config.actions';

export default compose(
  connect(state => ({ isSidebarOpened: state.Layout.isSidebarOpened })),
  lifecycle({
    componentWillMount() {
      this.props.dispatch(actionCreator(REQUEST(TYPE, 'getAgentData')));
    },
  })
)(View);
