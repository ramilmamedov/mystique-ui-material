import { REQUEST, SUCCESS, FAILURE } from 'utils/config.actions';

export const TYPE = `LAYOUT`;

export const toggleSidebar = () => ({
  type: TYPE,
});

export const initialState = {
  isSidebarOpened: true,
  agentData: {
    name: '',
    surname: '',
    paternal: '',
    birthday: '',
    phoneNumber: '',
  },
  request_login: false,
  success_login: false,
  failure_login: false,
  //
  failure_logout: false,
  request_logout: false,
  success_logout: false,
};

const Layout = (state = initialState, { type, payload }) => {
  switch (type) {
    case TYPE:
      return {
        ...state,
        isSidebarOpened: !state.isSidebarOpened,
      };
    case REQUEST(TYPE, 'login'):
      return {
        ...state,
        request_login: true,
      };
    case SUCCESS(TYPE, 'login'):
      return {
        ...state,
        request_login: false,
        success_login: true,
      };
    case FAILURE(TYPE, 'login'):
      return {
        ...state,
        request_login: false,
        failure_login: true,
      };
    case REQUEST(TYPE, 'getAgentData'):
      return {
        ...state,
        request_getAgentData: true,
      };
    case SUCCESS(TYPE, 'getAgentData'):
      const {
        name,
        surname,
        paternal,
        birthday,
        phoneNumber,
        status,
      } = payload;
      return {
        ...state,
        request_getAgentData: false,
        success_getAgentData: true,
        getAgentData: status,
        agentData: {
          name: name,
          surname: surname,
          paternal: paternal,
          birthday: birthday,
          phoneNumber: phoneNumber,
        },
      };
    case FAILURE(TYPE, 'getAgentData'):
      return {
        ...state,
        request_getAgentData: false,
        success_getAgentData: false,
      };
    default:
      return state;
  }
};

export default Layout;
