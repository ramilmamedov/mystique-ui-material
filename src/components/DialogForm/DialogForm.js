import React from 'react';
import { makeStyles } from '@material-ui/styles';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@material-ui/core';

const DialogForm = ({ children, title, open, closeModal, Buttons }) => {
  const classes = useStyles();
  return (
    <div>
      <Dialog
        open={open}
        scroll="body"
        onClose={closeModal}
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title">{title}</DialogTitle>
        <DialogContent dividers className={classes.dialogContent}>
          {children}
        </DialogContent>
        <DialogActions className={classes.dialogActions}>
          {Buttons}
        </DialogActions>
      </Dialog>
    </div>
  );
};

const useStyles = makeStyles({
  dialogContent: { padding: '16px 24px 42px' },
  dialogActions: { padding: '16px 24px', justifyContent: 'flex-start' },
});

export default React.memo(DialogForm);
